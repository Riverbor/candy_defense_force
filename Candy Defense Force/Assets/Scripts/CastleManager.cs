﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CastleManager : MonoBehaviour
{
    public Text HealthTextReference;
    public float StartHealth = 100f;
    public GameObject GameOverPanel;

    void Start()
    {
        //HealthTextReference.text = StartHealth.ToString();
    }

    public void GetDamage(float damage)
    {
        StartHealth -= damage;
        if(StartHealth <= 0)
        {
            GameOver();
        }
        if(HealthTextReference != null)
        {
            HealthTextReference.text = StartHealth.ToString();
        }
    }

    public void GameOver()
    {
        Time.timeScale = 0f;
        if(GameOverPanel != null)
        {
            GameOverPanel.SetActive(true);
        }
    }
}
