﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject ui;
    public string MenuToLoad = "MainMenu";

    public void Pause()
    {
        if (ui != null)
        {
            ui.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    public void Resume()
    {
        if (ui != null)
        {
            ui.SetActive(false);
            Time.timeScale = 1f;
        }
    }

    public void Menu()
    {
        if (ui != null)
        {
            ui.SetActive(false);
        }
        Time.timeScale = 1f;
        SceneManager.LoadScene(MenuToLoad);
    }

    public void Restart()
    {
        if (ui != null)
        {
            ui.SetActive(false);
        }
        Time.timeScale = 1f;
        SceneManager.LoadScene("Level1");
    }

}
