﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Transform target;

    public GameObject parentScene;

    public float speed = 70f;

    public GameObject impactEffect;

    public void Seek(Transform _target)
    {
        target = _target;
    }

    // Update is called once per frame
    void Update()
    {
        if(target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 direction = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;


        if(direction.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(direction.normalized * distanceThisFrame, Space.World);
    }

    void HitTarget()
    {
        GameObject effectInstance = Instantiate(impactEffect, transform.position, transform.rotation);
        if(parentScene == null)
        {
            parentScene = GameObject.Find("ImageTargetLevel1");
        }
        effectInstance.transform.parent = parentScene.transform;
        Destroy(effectInstance, 2f);

        Enemy enemy = target.GetComponent<Enemy>();
        if(enemy != null)
        {
            enemy.GetDamage(20f);
        }
        
        Destroy(gameObject);
    }
}
