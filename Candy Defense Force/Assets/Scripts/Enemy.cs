﻿using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{

    public float speed = 10f;

    private Transform target;
    private int wavepointIndex = 0;

    public float startHealth = 100f;
    public float moneyAtDeath = 50f;
    public float damageToCastle = 10f;
    private float health;


    public GameObject GameMasterReference;
    public Image healthBar;

    void Start()
    {
        target = Waypoints.points[0];
        health = startHealth;
    }

    void Update()
    {
        Vector3 direction = target.position - transform.position;
        transform.Translate(direction.normalized * speed * Time.deltaTime, Space.World);

        Quaternion lookRotation = Quaternion.LookRotation(direction);
        Vector3 rotation = Quaternion.Lerp(transform.rotation, lookRotation, Time.deltaTime * 20f).eulerAngles;
        transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);

        if (Vector3.Distance(transform.position, target.position) <= 0.4f)
        {
            GetNextWaypoint();
        }
    }

    public void GetDamage(float damage)
    {

        health -= damage;
        if(healthBar != null)
        {
            healthBar.fillAmount = health / startHealth;
        }
        

        if (health <= 0)
        {
            Destroy(gameObject);
            return;
        }
        
    }

    void GetNextWaypoint()
    {
        if (wavepointIndex >= Waypoints.points.Length - 1)
        {
            if (GameMasterReference == null)
            {
                GameMasterReference = GameObject.Find("GameMaster");
            }
            CastleManager GameMaster = GameMasterReference.GetComponent<CastleManager>();
            GameMaster.GetDamage(damageToCastle);


            Destroy(gameObject);
            return;
        }

        wavepointIndex++;
        target = Waypoints.points[wavepointIndex];

    }

}
//StateManager sm = TrackerManager.Instance.GetStateManager();

// Query the StateManager to retrieve the list of
// currently 'active' trackables
//(i.e. the ones currently being tracked by Vuforia)
//IEnumerable<TrackableBehaviour> activeTrackables = sm.GetActiveTrackableBehaviours()