﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public string LevelToLoad = "Level1";
    public GameObject partToRotate;

    public void Play()
    {
        SceneManager.LoadScene(LevelToLoad);
    }

    public void Quit()
    {
        Debug.Log("Quit");
    }

    void Update()
    {
        if(partToRotate != null)
        {
            partToRotate.transform.Rotate(0f, 100f * Time.deltaTime, 0f);
        }
    }
}
