﻿using System;
using UnityEngine;
using Vuforia;

public class MarkerTrack : MonoBehaviour, ITrackableEventHandler
{
    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if(previousStatus != TrackableBehaviour.Status.DETECTED && newStatus != TrackableBehaviour.Status.DETECTED)
        {
            Debug.Log("Marker not detected");
            GameObject.Find("PauseMenu").SetActive(true);
        }
        
    }
}
