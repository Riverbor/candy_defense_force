﻿using System;
using UnityEngine;

public class Turret : MonoBehaviour
{

    private Transform target;

    [Header("Attributes")]

    public float range = 10f;
    public float fireRate = 1f;
    private float fireCountDown = 0f;

    [Header("Unity Setup Fields")]
    
    public string enemyTag = "Enemy";
    public float turnSpeed = 20f;
    public Transform partToRotate;
    public Transform canonRightToRotate;
    public Transform canonLeftToRotate;

    public GameObject parentScene;
    public GameObject bulletPrefab;
    public Transform firePointLeft;
    public Transform firePointRight;

    private bool rightShootAlreadyLaunched = false;

    void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
    }

    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
        }
        else
        {
            target = null;
        }
    }

    void Update()
    {
        if (target == null)
            return;
        Vector3 direction = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);


        if(canonLeftToRotate != null)
        {
            canonLeftToRotate.Rotate(0f, 0f, 50f, Space.Self);
        }
        if(canonRightToRotate != null)
        {
            canonRightToRotate.Rotate(0f, 0f, -50f, Space.Self);
        }
        
        

        if (fireCountDown <= 0f)
        {
            Shoot();
            fireCountDown = 1f / fireRate;
        }
        fireCountDown -= Time.deltaTime;
    }

    private void Shoot()
    {
        Transform bulletGo;
        
        if (rightShootAlreadyLaunched)
        {
            
            bulletGo = Instantiate(bulletPrefab, firePointLeft.position, firePointLeft.rotation).transform;
            rightShootAlreadyLaunched = false;
        }
        else
        {
            bulletGo = Instantiate(bulletPrefab, firePointRight.position, firePointRight.rotation).transform;
            rightShootAlreadyLaunched = true;
        }

        if (parentScene != null)
        {
            bulletGo.transform.parent = parentScene.transform;
        }

        Bullet bullet = bulletGo.GetComponent<Bullet>();

        if(bullet != null)
        {
            bullet.Seek(target);
        }

    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }

}
