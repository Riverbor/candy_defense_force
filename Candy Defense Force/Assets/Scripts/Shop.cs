﻿using UnityEngine;

public class Shop : MonoBehaviour
{

    public TurretBlueprint UpgradeTurretModel;

    public GameObject ui;

    public void UpgradeTurret()
    {
        Debug.Log("Turret Upgraded");
    }

    public void Pause()
    {
        if (ui == null)
        {
            ui = GameObject.Find("PauseMenu");
        }
        ui.SetActive(true);
        Time.timeScale = 0f;
    }

}
