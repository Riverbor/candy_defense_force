﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour
{
    public GameObject parentScene;

    public Transform enemyPrefab;

    public Transform spawnPoint;

    public float timeBetweenWaves = 5f;
    private float countDown = 2f;

    private int waveIndex = 0;

    void Start()
    {

    }

    void Update()
    {
        if (countDown <= 0f)
        {
            StartCoroutine(SpawnWave());

            countDown = timeBetweenWaves;
        }
        countDown -= Time.deltaTime;
    }

    IEnumerator SpawnWave()
    {
        waveIndex++;

        //Debug.Log(string.Format("Wave {0} incomming !", waveIndex));
        for (int i = 0; i < waveIndex; i++)
        {
            SpawnEnemy();
            yield return new WaitForSeconds(0.5f);
        }
    }

    void SpawnEnemy()
    {
        Transform enemy = Instantiate(enemyPrefab, spawnPoint.position, spawnPoint.rotation);
        if (parentScene != null)
        {
            enemy.transform.parent = parentScene.transform;
        }
    }
}
